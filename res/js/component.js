sap.designstudio.sdk.Component.subclass("com.newcompany.box.Box", function() {
	var that = this;

	var column1_data = null;

	var _goal = null;

	this.init = function() {
		this.$().addClass("coloredBox");

		this.$().click(function() {
			that.fireEvent("onclick");
		});
	};

	this.color = function(value) {
		if (value === undefined) {
			return this.$().css("background-color");
		} else {
			this.$().css("background-color", value);
			return this;
		}
	};

	this.afterUpdate = function() {
		console.log(that);
		console.log(this.$());
		if (column1_data && column1_data.formattedData) {
			var data = column1_data.formattedData;
			var avg = 0;
			for (var count = 0; count < data.length; count++) {
				avg += parseFloat(data[count].replace(',', ''));
			}
			avg /= data.length;
			that.$().html("" + avg);
			console.log(this.$().context);
			d3.select(this.$().context).append("svg").attr("width", 500).attr("height", 500)
					.append("rect").attr("x", 25).attr("y", 25).attr("width",
							Math.round(avg)/1000).attr("height", 25).style("fill", "purple");
		}
	}

	this.column1 = function(value) {
		if (value === undefined) {
			return column1_data;
		} else {
			column1_data = value;
			return this;
		}
	};

	this.metadata = function(value) {
		if (value === undefined) {
			return meta_data;
		} else {
			meta_data = value;
			return this;
		}
	};

	this.goal = function(value) {
		if (value === undefined) {
			return meta_data;
		} else {
			meta_data = value;
			return this;
		}
	};
});
